#include "header.h"

int main(int argc, char *argv[])
{
	// Longitud del array de enteros
	int n;
	// Entrada - Cantidad de procesos
	int cp = atoi(argv[1]);
	// Entrada - String de numeros
	char *string = argv[2];
	// Convertir a array de enteros
		// Llamada a contador de comas
		n = contComas(string) + 1;
		int vec[n];
		
		converter(n, &vec[0], string);
		// Llamada a merge (en este punto ya tenemos el vec listo para trabajar)
		printf("===esquema de arbol===\n\n");
		imprimirArbolB(cp,vec,n);
		printf("\n\n===mapeos===\n");
		imprimirMapeo(cp,vec,n);
		
	// mergeSort(vec,0,n);
	return 0;
}
