#include "header.h"

void converter(int n, int *vec, char *string)
{
	int i = 0;
	char *token;

	token = strtok(string, ",");

	while (token != 0)
	{
		*(vec + i) = atoi(token);
		token = strtok(0, ",");
		i++;
	}
}

int contComas(char *ptrCad)
{
	int i, cont = 0;
	for (i = 0; i < strlen(ptrCad); i++)
	{
		if (ptrCad[i] == ',')
		{
			cont++;
		}
	}
	return cont;
}

void merge(int arr[], int l, int m, int r)
{
	int i, j, k;
	int n1 = m - l + 1;
	int n2 = r - m;

	/* create temp arrays */
	int L[n1], R[n2];

	/* Copy data to temp arrays L[] and R[] */
	for (i = 0; i < n1; i++)
		L[i] = arr[l + i];
	for (j = 0; j < n2; j++)
		R[j] = arr[m + 1 + j];

	/* Merge the temp arrays back into arr[l..r]*/
	i = 0; // Initial index of first subarray
	j = 0; // Initial index of second subarray
	k = l; // Initial index of merged subarray
	while (i < n1 && j < n2)
	{
		if (L[i] <= R[j])
		{
			arr[k] = L[i];
			i++;
		}
		else
		{
			arr[k] = R[j];
			j++;
		}
		k++;
	}

	/* Copy the remaining elements of L[], if there
	are any */
	while (i < n1)
	{
		arr[k] = L[i];
		i++;
		k++;
	}

	/* Copy the remaining elements of R[], if there
	are any */
	while (j < n2)
	{
		arr[k] = R[j];
		j++;
		k++;
	}
}

void mergeSort(int arr[], int l, int r)
{
	if (l < r)
	{
		// Same as (l+r)/2, but avoids overflow for
		// large l and h
		int m = l + (r - l) / 2;

		// Sort first and second halves
		mergeSort(arr, l, m);
		mergeSort(arr, m + 1, r);

		merge(arr, l, m, r);
	}
}

void imprimirArbolB(int cantidadProcesos,int array[],int longitudArray)
{
    

    int longitudArrayTemp = longitudArray;
    int nivelesArbol= log2f(cantidadProcesos)+1;
   int indiceTabulacion = nivelesArbol;
   int procesoActual = 0,tabulacionElementos = 0, nodosPorNivel;


   for(int nivelActual = 0; nivelActual < nivelesArbol; nivelActual++){
       indiceTabulacion--;
        nodosPorNivel = pow(2, nivelActual);
        for(int i = 0; i < indiceTabulacion; i++){
            printf("     \t\t");
        }

        for(int i = 0; i < nodosPorNivel; i++ ){
                printf("Proceso %d", procesoActual);
                printf("\t");
                procesoActual++;
        }
        printf("\n");

        if(nivelActual > 0){                                                    //---aca---
            if( (float)longitudArrayTemp / 2 > (int)(longitudArrayTemp / 2))
                tabulacionElementos = (longitudArrayTemp / 2) + 1;
            else
                tabulacionElementos = (longitudArrayTemp / 2);
        }
        for(int i = 0; i < indiceTabulacion; i++){
            printf("\t");
        }
        int contadorElementos = 0;
        for(int i = 0; i < longitudArray; i++){
            printf("%d", array[ i ]);
            if(i + 1 < longitudArray){
                printf(",");
            }
            contadorElementos++;
            if(contadorElementos == tabulacionElementos){
                printf("\t");
                contadorElementos = 0;
            }
        }
        if(nivelActual > 0){                                                    //----aca---
            if((float)longitudArrayTemp / 2 >= (int)(longitudArrayTemp / 2))
                longitudArrayTemp = tabulacionElementos - 1;
            else
                longitudArrayTemp = tabulacionElementos;
        }
        printf("\n\n");
    }
}

void imprimirMapeo(int cantidadProcesos,int array[],int longitudArray)
{
	
	

    int longitudArrayTemporal = longitudArray;
    int nivelesArbol= log2f(cantidadProcesos)+1;
   int indiceTabulacion = nivelesArbol;
   int procesoActual = 0,tabulacionElementos = 0, nodosPorNivel;

   for(int nivelActual = 0; nivelActual < nivelesArbol; nivelActual++){
       indiceTabulacion--;	
        nodosPorNivel = pow(2, nivelActual);


        if(nivelActual > 0){
            if( (float)longitudArrayTemporal / 2 > (int)(longitudArrayTemporal / 2))
                tabulacionElementos = (longitudArrayTemporal / 2) + 1;
            else
                tabulacionElementos = (longitudArrayTemporal / 2);
        }

        int contadorElementos = 0;	

        for(int i = 0; i < longitudArray; i++){

            if (contadorElementos == 0)
                printf("\nProceso %d: ", procesoActual),procesoActual++;

            printf("%d", array[ i ]);
            if(i + 1 < longitudArray){
                printf(",");
            }


            contadorElementos++;

            if(contadorElementos == tabulacionElementos){

                contadorElementos = 0;
                printf("\n");

            }
        }

        if(nivelActual > 0){
            if((float)longitudArrayTemporal / 2 > (int)(longitudArrayTemporal / 2))
                longitudArrayTemporal = tabulacionElementos - 1;
            else
                longitudArrayTemporal = tabulacionElementos;

        }

    }
}

