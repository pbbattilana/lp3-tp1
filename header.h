#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

void converter (int n, int *vec, char *string);
int contComas(char *prtCad);
void mergeSort(int arr[], int l, int r);
void merge(int arr[], int l, int m, int r);
void imprimirArbolB(int cantidadProcesos,int array[], int longitudArray);
void imprimirMapeo(int cantidadProcesos,int array[], int longitudArray);
